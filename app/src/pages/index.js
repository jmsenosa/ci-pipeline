import * as React from "react"

const marquee_style = {
  margin:"20px auto",
  width: "30%",
};

const p_style = {
  width: "50%",
  margin: "20px auto",
  textAlign: "center"
};

const img_style = {
    margin: "50px auto 80px",
    position: "relative",
    display: "block",
    background: "#edf0ab",
    padding: "48px 74px",
    width: 400,
    borderRadius: "50%"
}

const img = 'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/fb9cc1fd-4970-4659-b67c-e1628299afb8/dax8ad0-f7c9770d-d388-4d0f-85e9-19d48666749b.png?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcL2ZiOWNjMWZkLTQ5NzAtNDY1OS1iNjdjLWUxNjI4Mjk5YWZiOFwvZGF4OGFkMC1mN2M5NzcwZC1kMzg4LTRkMGYtODVlOS0xOWQ0ODY2Njc0OWIucG5nIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.iJyT3ercFMRPwDucHXd9gK8K7y5w7FGYnKlLcCHR-Bs';
// markup
const IndexPage = () => {
  return (
    <main>
      <div className="content">
          <p style={p_style}>this is jm's page</p>
          <div>
            <img style={img_style} src={img} alt="It was me, Dio!" srcset="" />
          </div>
          <div style={marquee_style}>
            <marquee behavior="alternate" direction="RIGHT">Yessss!!!???</marquee>
            <center>Using marquee on modern web development</center>
          </div>
      </div>
    </main>
  )
}

export default IndexPage
